package com.company;

public class Human {
    private String firstName;
    private String lastName;
    private String patronymic;

    public Human(String lastName, String firstName){
        this.lastName = lastName;
        this.firstName = firstName;
    }

    public Human(String lastName, String firstName, String patronymic){
        this.lastName = lastName;
        this.firstName = firstName;
        this.patronymic = patronymic;
    }

    public String getFullName(){
        String fullName;
        if ((this.lastName!=null)&&(this.firstName!=null)&&(this.patronymic!=null)){
            fullName = this.lastName + " " + this.firstName + " " + this.patronymic;
        } else
            if ((this.lastName!=null)&&(this.firstName!=null)&&(this.patronymic==null)){
                fullName = this.lastName + " " + this.firstName;
            } else fullName = "Неверно введены данные";
            return fullName;
    }

    public String getShortName(){
        String shortName;
        if ((this.lastName!=null)&&(this.firstName!=null)&&(this.patronymic!=null)){
            shortName = this.lastName + " " + this.firstName.substring(0,1) + ". " + this.patronymic.substring(0,1) + ".";
        } else
            if ((this.lastName!=null)&&(this.firstName!=null)&&(this.patronymic==null)){
                shortName = this.lastName + " " + this.firstName.substring(0,1) + ".";
        } else shortName = "Неверно введены данные";
            return shortName;
    }
}
